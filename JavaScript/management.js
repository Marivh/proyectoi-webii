var app = new Vue({
    el: "#app",
    data: {
      channels: [],
      channel: {},
      tempChannel: {
        name: "",
        index: null,
      },
      serviceId: "",
    },
    //Para cargar métodos y variables una vez que se inicie la pagina
    created() {
      this.serviceId = localStorage.getItem("ServiceId");
      this.getChannels();
    },
    methods: {
      //Metodo para agregar un canal, se le envia el Id del servicio y como parámetro el nombre del canal.
      addChannel() {
        var bodyFormData = new FormData();
        bodyFormData.set("FriendlyName", this.tempChannel.name);

        axios({
            method: "post",
            url: `https://chat.twilio.com/v2/Services/${this.serviceId}/Channels`,
            data: bodyFormData,
            headers: {
              "Content-Type": "multipart/form-data"
            },
            auth: {
              username: ACCOUNTSID,
              password: AUTHTOKEN,
            },
          })
          .then((response) => {
            this.channels.push(response.data);
            this.tempChannel.name = "";
          })
          .catch((e) => {
            // this.errors.push(e);
            console.log(e);
          })
          .finally(() => {});
      },

      //Método para obtener la lista de canales.
      getChannels() {
        axios({
            method: "get",
            url: `https://chat.twilio.com/v2/Services/${this.serviceId}/Channels`,
            auth: {
              username: ACCOUNTSID,
              password: AUTHTOKEN,
            },
          })
          .then((response) => {
            // JSON responses are automatically parsed.
            this.channels = response.data.channels;
          })
          .catch((e) => {
            // this.errors.push(e);
            console.log(e);
          })
          .finally(() => {});
      },

      //Método para editar el canal.
      editChannel(index) {
        var bodyFormData = new FormData();
        bodyFormData.set("FriendlyName", this.tempChannel.name);

        axios({
            method: "post",
            url: `https://chat.twilio.com/v2/Services/${this.serviceId}/Channels/${this.channels[index].sid}`,
            data: bodyFormData,
            headers: {
              "Content-Type": "multipart/form-data"
            },
            auth: {
              username: ACCOUNTSID,
              password: AUTHTOKEN,
            },
          })
          .then((response) => {
            this.channels.splice(index, 1, response
              .data); //Reemplazar con el response.data el valor en la posicion index de la lista
            this.tempChannel.name = "";
          })
          .catch((e) => {
            // this.errors.push(e);
            console.log(e);
          })
          .finally(() => {});
      },

      /*Método que valida si el campo no está vacío, y además verifica si hay un index seleccionado, si el index es diferente de null
      debe usar el método de editar y si no debe usar el método de agregar.*/
      save() {
        if (
          !(
            this.tempChannel.name == "" ||
            this.tempChannel.name == null ||
            this.tempChannel.name == " "
          )
        ) {
          if (this.tempChannel.index != null) {
            console.log("edit");
            this.editChannel(this.tempChannel.index);
            this.tempChannel.index = null;
          } else {
            console.log("add");
            this.addChannel();
            this.tempChannel.index = null;
          }
        }
      },

      //Método para seleccionar un canal.
      selectChannel(index) {
        this.tempChannel.name = this.channels[index].friendly_name;
        this.tempChannel.index = index;
      },

      //Método para salir.
      salir() {
        window.location.replace("./user.html");
      },

      //Método para eliminar un canal.
      deleteChannel(index) {
        axios({
            method: "delete",
            url: `https://chat.twilio.com/v2/Services/${this.serviceId}/Channels/${this.channels[index].sid}`,
            auth: {
              username: ACCOUNTSID,
              password: AUTHTOKEN,
            },
          })
          .then((response) => {
            // JSON responses are automatically parsed.
            this.channels.splice(index, 1);
          })
          .catch((e) => {
            // this.errors.push(e);
            console.log(e);
          })
          .finally(() => {});
      },
    },
  });