var app = new Vue({

    el: "#app",
    data: {
        members: [],
        member: {},
        messages: [],
        message: {},
        tempChannel: {
            name: "",
            index: null,
        },
        serviceId: "",
        identity: "",
        channelId: "",
        labelMessageIdentity: "",
        bodyMessage: "",
        channelName: "",
    },
    //Para cargar metodos los metodos y la información de las variables apenas se abra la página.
    created() {
        var expresionRegular = /\s*Z\s*/;
        this.channelId = sessionStorage.getItem('channelId');
        this.serviceId = localStorage.getItem("ServiceId");
        this.identity = sessionStorage.getItem("identity");
        this.getMessages();
        this.getMembers();
        this.getChannelName();

    },
    methods: {
        //Metodo para obtener los mensajes de ese canal, se le envia el ID del servicio y el ID del canal.
        getMessages() {
            axios({
                    method: "get",
                    url: `https://chat.twilio.com/v2/Services/${this.serviceId}/Channels/${this.channelId}/Messages`,
                    auth: {
                        username: ACCOUNTSID,
                        password: AUTHTOKEN,
                    },
                })
                .then((response) => {
                    // JSON responses are automatically parsed.
                    this.messages = response.data.messages;

                    this.messages.forEach(message => {
                       /* if (message.from == this.identity) {
                            document.getElementById("prueba").className = "message-box-prueba";
                        }*/
                    });
                })
                .catch((e) => {
                    // this.errors.push(e);
                    console.log(e);
                })
                .finally(() => {});
        },

        //Método para obtener los miembros de ese canal, se le envia el ID del servicio y el ID del canal
        getMembers() {
            axios({
                    method: "get",
                    url: `https://chat.twilio.com/v2/Services/${this.serviceId}/Channels/${this.channelId}/Members`,
                    auth: {
                        username: ACCOUNTSID,
                        password: AUTHTOKEN,
                    },
                })
                .then((response) => {
                    // JSON responses are automatically parsed.
                    this.members = response.data.members;
                })
                .catch((e) => {
                    // this.errors.push(e);
                    console.log(e);
                })
                .finally(() => {});
        },

        //Método para obtener el nombre del canal.
        getChannelName() {
            axios({
                    method: "get",
                    url: `https://chat.twilio.com/v2/Services/${this.serviceId}/Channels/${this.channelId}`,
                    auth: {
                        username: ACCOUNTSID,
                        password: AUTHTOKEN,
                    },
                })
                .then((response) => {
                    // JSON responses are automatically parsed.
                    this.channelName = response.data.friendly_name;
                    var objetivo = document.getElementById('title');
                    objetivo.innerHTML += this.channelName;

                })
                .catch((e) => {
                    // this.errors.push(e);
                    console.log(e);
                })
                .finally(() => {});
        },

        //Método para salir y destruir el sessionStorage para redirigir a la pagina de inicio.
        salir() {
            sessionStorage.removeItem("identity");
            sessionStorage.removeItem("channelId");
            window.location.replace("./user.html");
        },

        //Metodo para crear un mensaje, se le envia el ID del servicio y el ID del canal, además de los parámetros del body y del from.
        createMessage() {
            var bodyFormData = new FormData();
            bodyFormData.set("Body", this.bodyMessage);
            bodyFormData.set("From", this.identity);

            axios({
                    method: "post",
                    url: `https://chat.twilio.com/v2/Services/${this.serviceId}/Channels/${this.channelId}/Messages`,
                    data: bodyFormData,
                    headers: {
                        "Content-Type": "multipart/form-data"
                    },
                    auth: {
                        username: ACCOUNTSID,
                        password: AUTHTOKEN,
                    },
                })
                .then((response) => {
                    location.reload();
                })
                .catch((e) => {
                    // this.errors.push(e);
                    console.log(e);
                })
                .finally(() => {});
        }

    },
});