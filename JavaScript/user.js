var app = new Vue({
    el: "#app",
    data: {
        channels: [],
        channel: {},
        tempChannel: {
            name: "",
            index: null,
        },
        serviceId: "",
        identity: "",
        identityId: "",
    },
    created() {
        this.identity = sessionStorage.getItem("identity");
        this.serviceId = localStorage.getItem("ServiceId");
        this.getListChannels();
    },
    methods: {
        //Metodo para obtener la lista de canales y se guardan en el objeto channels.
        getListChannels() {
            axios({
                    method: "get",
                    url: `https://chat.twilio.com/v2/Services/${this.serviceId}/Channels`,
                    auth: {
                        username: ACCOUNTSID,
                        password: AUTHTOKEN,
                    },
                })
                .then((response) => {
                    // JSON responses are automatically parsed.
                    this.channels = response.data.channels;
                })
                .catch((e) => {
                    // this.errors.push(e);
                    console.log(e);
                })
                .finally(() => {});
        },

        /**Metodo para unirse a un canal, se le envia el ID del servicio y el Id del canal seleccionado y si todo está bien se 
        obtiene la identidad y el canal seleccionado para guardarlo en el sessionStorage*/
        joinChannel(index) {
            if (this.identity == null) {
                var objetivo = document.getElementById('fail');
                objetivo.innerHTML += "Debes agregar un nickname para poder ingresar";
            } else {
                var bodyFormData = new FormData();
                bodyFormData.set("Identity", this.identity);

                axios({
                        method: "post",
                        url: `https://chat.twilio.com/v2/Services/${this.serviceId}/Channels/${this.channels[index].sid}/Members`,
                        data: bodyFormData,
                        headers: {
                            "Content-Type": "multipart/form-data"
                        },
                        auth: {
                            username: ACCOUNTSID,
                            password: AUTHTOKEN,
                        },
                    })
                    .then((response) => {
                        this.identity = response.data.identity;
                        sessionStorage.setItem("identity", this.identity);
                        sessionStorage.setItem("channelId", this.channels[index].sid);
                        window.location.replace("./channelChat.html");
                    })
                    .catch((e) => {
                        // this.errors.push(e);
                        console.log(e);
                    })
                    .finally(() => {});
            }
        },
    },
});